
CanIMogItOptions = {
	["debug"] = false,
	["showUnknownOnly"] = true,
	["showTransmoggableOnly"] = true,
	["showItemIconOverlay"] = true,
	["iconLocation"] = "BOTTOMLEFT",
	["showEquippableOnly"] = true,
	["version"] = "20",
	["printDatabaseScan"] = false,
	["databaseDebug"] = false,
	["showSourceLocationTooltip"] = false,
	["showVerboseText"] = true,
	["showSetInfo"] = true,
}
CanIMogItDatabase = {
}
