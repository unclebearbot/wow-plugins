
MapsterDB = {
	["namespaces"] = {
		["FogClear"] = {
		},
		["Coords"] = {
			["profiles"] = {
				["Default"] = {
					["accuracy"] = 2,
				},
			},
		},
	},
	["profileKeys"] = {},
	["profiles"] = {
		["Default"] = {
			["arrowScale"] = 1,
			["ejScale"] = 1,
			["poiScale"] = 1,
			["hideMapButton"] = true,
			["y"] = 20,
			["x"] = 40,
		},
	},
}
